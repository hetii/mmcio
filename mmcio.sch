EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mmcio-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X08 P1
U 1 1 59551A7D
P 1600 1200
F 0 "P1" H 1600 1650 50  0000 C CNN
F 1 "IO_1" V 1700 1150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04" H 1600 1200 50  0001 C CNN
F 3 "" H 1600 1200 50  0000 C CNN
	1    1600 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 850  2400 850 
Wire Wire Line
	1800 950  2400 950 
Wire Wire Line
	1800 1050 2400 1050
Wire Wire Line
	1800 1150 2400 1150
Wire Wire Line
	1800 1250 2400 1250
Wire Wire Line
	1800 1350 2400 1350
Wire Wire Line
	1800 1450 2400 1450
Wire Wire Line
	1800 1550 2400 1550
Text Label 1800 850  0    60   ~ 0
DAT1/IRQ
Text Label 1800 950  0    60   ~ 0
DAT0/MISO
Text Label 1800 1050 0    60   ~ 0
GND
Text Label 1800 1150 0    60   ~ 0
CLK/SCK
Text Label 1800 1250 0    60   ~ 0
VDD
Text Label 1800 1350 0    60   ~ 0
CMD/MOSI
Text Label 1800 1550 0    60   ~ 0
DAT2
Text Label 1800 1450 0    60   ~ 0
CD/DATA3/CS
$Comp
L micro-sdcard CON1
U 1 1 59553ABF
P 2900 1250
F 0 "CON1" H 3044 633 50  0000 C CNN
F 1 "micro-sdcard" H 3044 724 50  0000 C CNN
F 2 "kicadlib:micro-sdcard" H 4050 1550 50  0001 C CNN
F 3 "" H 2900 1250 50  0000 C CNN
	1    2900 1250
	1    0    0    1   
$EndComp
$Comp
L CONN_01X08 P2
U 1 1 595582E8
P 4200 1250
F 0 "P2" H 4200 1700 50  0000 C CNN
F 1 "IO_2" V 4300 1200 50  0000 C CNN
F 2 "kicadlib:Pin_Header_Odroid" H 4200 1250 50  0001 C CNN
F 3 "" H 4200 1250 50  0000 C CNN
	1    4200 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 900  5000 900 
Wire Wire Line
	4400 1000 5000 1000
Wire Wire Line
	4400 1100 5000 1100
Wire Wire Line
	4400 1200 5000 1200
Wire Wire Line
	4400 1300 5000 1300
Wire Wire Line
	4400 1400 5000 1400
Wire Wire Line
	4400 1500 5000 1500
Wire Wire Line
	4400 1600 5000 1600
$Comp
L micro-sdcard CON2
U 1 1 595582FE
P 5500 1300
F 0 "CON2" H 5644 683 50  0000 C CNN
F 1 "micro-sdcard" H 5644 774 50  0000 C CNN
F 2 "kicadlib:micro-sdcard" H 6650 1600 50  0001 C CNN
F 3 "" H 5500 1300 50  0000 C CNN
	1    5500 1300
	1    0    0    1   
$EndComp
$EndSCHEMATC
